gcc c_utils.c -fPIC -shared -o c_utils.so  -std=c99
gcc c_textproposalconnector.c  -fPIC -shared -o c_textproposalconnector.so -std=c99
gcc c_proposallayer.c  c_utils.c  -fPIC -shared -o c_proposallayer.so -std=c99
gcc c_datalayer.c c_utils.c -fPIC -shared -o c_datalayer.so -std=c99